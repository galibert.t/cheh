import { useState } from 'react'
import './App.css'
import cheh from './assets/cheh.webp'

function App() {

  return (
    <div className="App">
      <img src={cheh} />
    </div>
  )
}

export default App
